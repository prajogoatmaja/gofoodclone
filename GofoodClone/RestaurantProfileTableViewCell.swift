//
//  RestaurantProfileTableViewCell.swift
//  GofoodClone
//
//  Created by Prajogo Atmaja on 13/04/20.
//  Copyright © 2020 Prajogo Atmaja. All rights reserved.
//

import UIKit

class RestaurantProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    func setRestaurant(restaurant: Restaurant) {
        restaurantImageView.image = restaurant.image
        restaurantNameLabel.text = restaurant.restaurantName
    }
}
