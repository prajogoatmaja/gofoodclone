//
//  Restaurant.swift
//  GofoodClone
//
//  Created by Prajogo Atmaja on 13/04/20.
//  Copyright © 2020 Prajogo Atmaja. All rights reserved.
//

import Foundation
import UIKit

class Restaurant {
    
    var image: UIImage
    var restaurantName : String
    
    init(image: UIImage, restaurantName: String) {
        self.image = image
        self.restaurantName = restaurantName
    }
}
