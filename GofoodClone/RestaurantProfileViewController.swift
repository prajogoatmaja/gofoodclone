//
//  RestaurantProfileViewController.swift
//  GofoodClone
//
//  Created by Prajogo Atmaja on 13/04/20.
//  Copyright © 2020 Prajogo Atmaja. All rights reserved.
//

import UIKit

class RestaurantProfileViewController: UIViewController {
    
    var restaurant: Restaurant?
    
    @IBOutlet weak var restaurantProfileTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restaurantProfileTableView.register(UINib(nibName: "RestaurantProfileTableViewCell", bundle: nil), forCellReuseIdentifier: "RestaurantProfileTableViewCell")

        restaurantProfileTableView.delegate = self
        restaurantProfileTableView.dataSource = self
    }
}
extension RestaurantProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        if indexPath.section == 0 {
            identifier = "RestaurantProfileTableViewCell"
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RestaurantProfileTableViewCell
            cell.setRestaurant(restaurant: restaurant!)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 400
        } else {
            return 60
        }
    }
}

