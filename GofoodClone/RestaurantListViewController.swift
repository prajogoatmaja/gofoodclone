//
//  RestaurantListViewController.swift
//  GofoodClone
//
//  Created by Prajogo Atmaja on 13/04/20.
//  Copyright © 2020 Prajogo Atmaja. All rights reserved.
//

import UIKit

class RestaurantListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "ToRestaurantProfileSegue") {
            if let restaurantProfileViewController = segue.destination as? RestaurantProfileViewController {
                if let selected = tableView.indexPathForSelectedRow {
                    let index = selected.row
                    let restaurant = restaurants[index]
                    restaurantProfileViewController.restaurant = restaurant
                }
            }
        }
    }
    
    var restaurants: [Restaurant] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        restaurants = createArray()
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func createArray() -> [Restaurant] {
        
        var tempRestaurants: [Restaurant] = []
        
        let restaurant1 = Restaurant(image: UIImage(named: "mie-ayam")!, restaurantName: "Mie ayam Permana, Kebagusan")
        
        let restaurant2 = Restaurant(image: UIImage(named: "mie-ayam")!, restaurantName: "Test Restoran")
        
        tempRestaurants.append(restaurant1)
        tempRestaurants.append(restaurant2)
        return tempRestaurants
    }
}

extension RestaurantListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restaurant = restaurants[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RestaurantTableViewCell") as! RestaurantTableViewCell
        
        cell.setRestaurant(restaurant: restaurant)
        return cell
    }
}
